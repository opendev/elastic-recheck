# All Rights Reserved.
#
#    Licensed under the Apache License, Version 2.0 (the "License"); you may
#    not use this file except in compliance with the License. You may obtain
#    a copy of the License at
#
#         http://www.apache.org/licenses/LICENSE-2.0
#
#    Unless required by applicable law or agreed to in writing, software
#    distributed under the License is distributed on an "AS IS" BASIS, WITHOUT
#    WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied. See the
#    License for the specific language governing permissions and limitations
#    under the License.
from functools import lru_cache
import logging
import os
import pytest

from launchpadlib import launchpad

import elastic_recheck.config as er_conf
from elastic_recheck import elasticRecheck
import elastic_recheck.query_builder as qb
# from elastic_recheck import tests

LPCACHEDIR = os.path.expanduser('~/.launchpadlib/cache')
LOGGER = logging.getLogger(__name__)


class Context():

    @lru_cache()
    def __init__(self):
        config = er_conf.Config(config_file='elasticRecheck.conf')
        self.classifier = elasticRecheck.Classifier(
            config.gerrit_query_file,
            config=config)
        self.lp = launchpad.Launchpad.login_anonymously(
            'grabbing bugs',
            'production',
            LPCACHEDIR)

        self.openstack_projects = (
            self.get_group_projects('openstack') +
            self.get_group_projects('oslo') +
            # Fix for story 2006737 since os-brick is
            # not in the openstack group in launchpad.
            ['os-brick'])

    def get_group_projects(self, group_name):
        group = self.lp.project_groups[group_name]
        return list(map(lambda project: project.name,
                        group.projects))

    def _is_valid_launchpad_bug(self, bug) -> bool:
        bug_tasks = self.lp.bugs[bug].bug_tasks
        projects = map(lambda bug_task: bug_task.bug_target_name, bug_tasks)
        # Check that at least one bug_tasks is targeted to an OpenStack Project
        for project in projects:
            if '/' in project:
                # project is a specific series, ignore
                continue
            if project in self.openstack_projects:
                return True
        LOGGER.error("bug has no targets in the openstack launchpad group")
        return False

    def _is_valid_ElasticSearch_query(self, x, bug) -> bool:
        query = qb.generic(x['query'])
        results = self.classifier.es.search(query, size='10')

        valid_query = len(results) > 0
        if not valid_query:
            LOGGER.error("Didn't find any hits for bug %s", x['bug'])
        # don't fail tests if no hits for a bug
        return True


_ctx = Context()


@pytest.fixture(scope='session')
def ctx(request):
    return _ctx


@pytest.mark.parametrize(
    "x",
    _ctx.classifier.queries,
    ids=[x['bug'] for x in _ctx.classifier.queries])
def test_launchpad(ctx, x):
    """Make sure queries are valid.

    Make sure all queries are for open OpenStack bugs in Launchpad or have
    hits in logstash.openstack.org.
    This test is used to validate any changes to queries.yaml
    """
    LOGGER.debug(
        "Looking for bug: https://bugs.launchpad.net/bugs/%s",
        x['bug'])
    assert \
        ctx._is_valid_launchpad_bug(x['bug']) is True, \
        f"Bug {x['bug']} is not targeted to openstack on launchpad."


@pytest.mark.parametrize(
    "x",
    _ctx.classifier.queries,
    ids=[x['bug'] for x in _ctx.classifier.queries])
def test_elasticsearch_query(ctx, x):
    LOGGER.debug(
        "Looking for bug: https://bugs.launchpad.net/bugs/%s",
        x['bug'])
    try:
        assert ctx._is_valid_ElasticSearch_query(x, x['bug']), \
            ("Something is wrong with bug %s" % x['bug'])
    except Exception as e:
        pytest.fail(f"Exception {e} receive with query: {x}")
