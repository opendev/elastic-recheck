import os
import sys
from typing import Optional


def atomic_write(filename: Optional[str], data: str) -> None:
    """Atomically writes a file.

    If filename is not mentioned, it will write to sys.stdout
    """
    if filename:
        with open(f"{filename}.tmp", "w") as f:
            f.write(data)
        os.replace(f"{filename}.tmp", filename)
    else:
        sys.stdout.write(data)
