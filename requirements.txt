pbr>=1.8
python-dateutil>=2.0
pytz
pyelasticsearch<1.0
gerritlib
python-daemon>=2.2.0
irc>=17.0
pyyaml
lockfile
Babel>=0.9.6
lazr.restfulclient>=0.14.2 # LGPL
httplib2>=0.12.0 # MIT License
launchpadlib>=1.10.6 # LGPL
Jinja2
requests
subunit2sql>=0.9.0
SQLAlchemy>=0.9.7,!=1.1.5,!=1.1.6,!=1.1.7,!=1.1.8
PyMySQL>=0.6.2 # MIT License
