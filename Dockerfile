# syntax=docker/dockerfile:experimental
FROM opendevorg/python-builder:3.7 as elastic-recheck-builder

COPY . /tmp/src
RUN assemble

FROM opendevorg/python-base:3.7 as elastic-recheck

COPY --from=elastic-recheck-builder /output/ /output
RUN /output/install-from-bindep && \
rm -rf /output
COPY data/  /data/
COPY queries/ /opt/elastic-recheck/queries

# using root allows us to use same relative paths in configs for running outside
# containers, where ./data contains persistent configs and logs.
WORKDIR /
CMD /usr/local/bin/elastic-recheck -f data/elastic-recheck.conf ${ER_OPTS:-}
