.EXPORT_ALL_VARIABLES:

PYTHON ?= $(shell command -v python3 python|head -n1)
# Keep docker before podman due to:
# https://github.com/containers/podman/issues/7602
ENGINE ?= $(shell command -v docker podman|head -n1)
# localhost/ prefix must be present in order to assure docker/podman compatibility:
# https://github.com/containers/buildah/issues/1034
IMAGE_TAG=localhost/elastic-recheck
# Enable newer docker buildkit if available
DOCKER_BUILDKIT=1

.PHONY: default
default: help

define PRINT_HELP_PYSCRIPT
import re, sys

print("Usage: make <target>\n")
cmds = {}
for line in sys.stdin:
	match = re.match(r'^([a-zA-Z_-]+):.*?## (.*)$$', line)
	if match:
	  target, help = match.groups()
	  cmds.update({target: help})
for cmd in sorted(cmds):
		print(f"  {cmd:12} {cmds[cmd]}")
endef
export PRINT_HELP_PYSCRIPT

.PHONY: help
help:
	@$(PYTHON) -c "$$PRINT_HELP_PYSCRIPT" < $(MAKEFILE_LIST)

.PHONY: build
build:  ## Build image using docker
	$(ENGINE) build -t $(IMAGE_TAG) .
	@echo "Image size: $$(docker image inspect --format='scale=0; {{.Size}}/1024/1024' $(IMAGE_TAG) | bc)MB"

.PHONY: into
into: ## Starts bash inside docker image
	$(ENGINE) run -it $(IMAGE_TAG) /bin/bash

.PHONY: dive
dive:  ## Use `dive` tool to investigate container size
	# https://github.com/wagoodman/dive
	dive $(IMAGE_TAG)
